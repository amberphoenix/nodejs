const { FighterRepository } = require("../repositories/fighterRepository");
const ErrorWithStatusCode = require("../helpers/ErrorWithStatusCode");
const statusCodes = require("../constants/statusCodes");

class FighterService {
  getAll() {
    const fighters = FighterRepository.getAll();
    if (!fighters.length) {
      throw new ErrorWithStatusCode(
        "No fighters in database",
        statusCodes.notFound
      );
    }
    return { data: fighters, statusCode: statusCodes.ok };
  }

  getById(id) {
    const fighter = FighterRepository.getOne(id);
    if (!fighter) {
      throw new ErrorWithStatusCode("Fighter not found", statusCodes.notFound);
    }
    return { data: fighter, statusCode: statusCodes.ok };
  }

  create(data) {
    const newFighter = FighterRepository.create(data);
    if (!newFighter) {
      throw new ErrorWithStatusCode(
        "Failed to create new fighter",
        statusCodes.notValid
      );
    }
    return { data: newFighter, statusCode: statusCodes.ok };
  }

  update(id, dataToUpdate) {
    const updatedFighter = FighterRepository.update(id, dataToUpdate);
    if (!updatedFighter) {
      throw new ErrorWithStatusCode(
        "Failed to update fighter",
        statusCodes.notValid
      );
    }
    return { data: updatedFighter, statusCode: statusCodes.ok };
  }

  delete(id) {
    const deletedFighter = FighterRepository.delete(id);
    if (!deletedFighter) {
      throw new ErrorWithStatusCode(
        "Failed to delete fighter",
        statusCodes.notValid
      );
    }
    return { data: deletedFighter, statusCode: statusCodes.ok };
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();
