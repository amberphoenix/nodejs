const UserService = require("./userService");
const ErrorWithStatusCode = require("../helpers/ErrorWithStatusCode");
const statusCodes = require("../constants/statusCodes");

class AuthService {
  login(userData) {
    const user = UserService.search(userData);
    if (!user) {
      throw ErrorWithStatusCode("User not found", statusCodes.notFound);
    }
    return { data: user, statusCode: statusCodes.ok };
  }
}

module.exports = new AuthService();
