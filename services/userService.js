const { UserRepository } = require("../repositories/userRepository");
const ErrorWithStatusCode = require("../helpers/ErrorWithStatusCode");
const statusCodes = require("../constants/statusCodes");

class UserService {
  getAll() {
    const users = UserRepository.getAll();
    if (!users.length) {
      throw new ErrorWithStatusCode(
        "No users in database",
        statusCodes.notFound
      );
    }
    return { data: users, statusCode: statusCodes.ok };
  }

  getById(id) {
    const user = UserRepository.getOne(id);
    if (!user) {
      throw new ErrorWithStatusCode("User not found", statusCodes.notFound);
    }
    return { data: user, statusCode: statusCodes.ok };
  }

  create(data) {
    const newUser = UserRepository.create(data);
    if (!newUser) {
      throw new ErrorWithStatusCode(
        "Failed to create new user",
        statusCodes.notValid
      );
    }
    return { data: newUser, statusCode: statusCodes.ok };
  }

  update(id, dataToUpdate) {
    const updatedUser = UserRepository.update(id, dataToUpdate);
    if (!updatedUser) {
      throw new ErrorWithStatusCode(
        "Failed to update user",
        statusCodes.notValid
      );
    }
    return { data: updatedUser, statusCode: statusCodes.ok };
  }

  delete(id) {
    const deletedUser = UserRepository.delete(id);
    if (!deletedUser) {
      throw new ErrorWithStatusCode(
        "Failed to delete user",
        statusCodes.notValid
      );
    }
    return { data: deletedUser, statusCode: statusCodes.ok };
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
