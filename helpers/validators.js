const ErrorWithStatusCode = require("../helpers/ErrorWithStatusCode");
const statusCodes = require("../constants/statusCodes");
const regex = require("../constants/regex");

exports.password = (password) => {
  if (password.length < 3)
    throw new ErrorWithStatusCode(
      "Password must be at least 3 symbols long",
      statusCodes.notValid
    );
};

exports.email = (email) => {
  if (!regex.email.test(String(email).toLowerCase()))
    throw new ErrorWithStatusCode(
      "Only gmail inbox is allowed",
      statusCodes.notValid
    );
};

exports.phoneNumber = (phoneNumber) => {
  if (!regex.phoneNumber.test(String(phoneNumber).toLowerCase()))
    throw new ErrorWithStatusCode(
      "Only Ukrainian phone number",
      statusCodes.notValid
    );
};

exports.lessThan = (limit, stat) => {
  if (isNaN(stat) || stat > limit)
    throw new ErrorWithStatusCode(
      "Incorrect fighter stats enetered",
      statusCodes.notValid
    );
};

exports.greaterThan = (limit, stat) => {
  if (isNaN(stat) || stat < limit)
    throw new ErrorWithStatusCode(
      "Incorrect fighter stats enetered",
      statusCodes.notValid
    );
};

exports.containsAllFields = (entityCandidate, entity) => {
  const { id, ...requiredFields } = entity;
  const candidateFields = Object.keys(entityCandidate);

  Object.keys(requiredFields).forEach((key) => {
    if (!candidateFields.includes(key))
      throw new ErrorWithStatusCode(
        `Entity missing field ${key}`,
        statusCodes.notValid
      );
  });
};

exports.hasExtraFields = (entityCandidate, entity) => {
  const entityFields = Object.keys(entity).filter((k) => k !== "id");

  Object.keys(entityCandidate).forEach((key) => {
    if (!entityFields.includes(key))
      throw new ErrorWithStatusCode(
        `Entity has extra field ${key}`,
        statusCodes.notValid
      );
  });
};

exports.hasEmptyFields = (entityCandidate) => {
  Object.keys(entityCandidate).forEach((key) => {
    if (!entityCandidate[key])
      throw new ErrorWithStatusCode(
        `Entity has empty field ${key}`,
        statusCodes.notValid
      );
  });
};

exports.unique = (searchFn, fieldName, fieldValue) => {
  if (searchFn({ [fieldName]: fieldValue }))
    throw new ErrorWithStatusCode(
      `${fieldName} ${fieldValue} is not unique`,
      statusCodes.notValid
    );
};
