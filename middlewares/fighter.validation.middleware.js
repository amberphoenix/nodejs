const { fighter } = require("../models/fighter");
const {
  containsAllFields,
  hasExtraFields,
  hasEmptyFields,
  unique,
  lessThan,
  greaterThan,
} = require("../helpers/validators");
const fighterService = require("../services/fighterService");

const createFighterValid = (req, res, next) => {
  try {
    const fighterCandidate = req.body;
    containsAllFields(fighterCandidate, fighter);
    hasExtraFields(fighterCandidate, fighter);
    hasEmptyFields(fighterCandidate);
    unique(fighterService.search, "name", fighterCandidate.name);
    lessThan(100, fighterCandidate.health);
    greaterThan(1, fighterCandidate.health);
    lessThan(100, fighterCandidate.power);
    greaterThan(1, fighterCandidate.power);
    lessThan(10, fighterCandidate.defense);
    greaterThan(1, fighterCandidate.defense);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateFighterValid = (req, res, next) => {
  try {
    const fighterCandidate = req.body;
    hasExtraFields(fighterCandidate, fighter);
    hasEmptyFields(fighterCandidate);
    if (fighterCandidate.name)
      unique(fighterService.search, "name", fighterCandidate.name);
    if (fighterCandidate.health) {
      lessThan(100, fighterCandidate.health);
      greaterThan(1, fighterCandidate.health);
    }
    if (fighterCandidate.power) {
      lessThan(100, fighterCandidate.power);
      greaterThan(1, fighterCandidate.power);
    }
    if (fighterCandidate.defense) {
      lessThan(10, fighterCandidate.defense);
      greaterThan(1, fighterCandidate.defense);
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
