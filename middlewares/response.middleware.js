const responseMiddleware = (req, res, next) => {
  if (res.err) {
    res.status(res.err.statusCode).send({
      error: true,
      message: res.err.message || "Unknown error",
    });
  } else {
    res.status(res.statusCode).send(res.data);
  }
  next();
};

exports.responseMiddleware = responseMiddleware;
