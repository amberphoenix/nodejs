module.exports = {
  ok: 200,
  notValid: 400,
  notFound: 404,
};
