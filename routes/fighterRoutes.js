const { Router } = require("express");
const {
  getFighters,
  getFighterById,
  createFighter,
  updateFighter,
  deleteFighter,
} = require("../controllers/fighterControllers");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

router.get("/", getFighters, responseMiddleware);
router.get("/:id", getFighterById, responseMiddleware);
router.post("/", createFighterValid, createFighter, responseMiddleware);
router.put("/:id", updateFighterValid, updateFighter, responseMiddleware);
router.delete("/:id", deleteFighter, responseMiddleware);

module.exports = router;
