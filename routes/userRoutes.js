const { Router } = require("express");
const {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
} = require("../controllers/userControllers");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get("/", getUsers, responseMiddleware);
router.get("/:id", getUserById, responseMiddleware);
router.post("/", createUserValid, createUser, responseMiddleware);
router.put("/:id", updateUserValid, updateUser, responseMiddleware);
router.delete("/:id", deleteUser, responseMiddleware);

module.exports = router;
