const FighterService = require("../services/fighterService");

exports.getFighters = (req, res, next) => {
  try {
    const { data, statusCode } = FighterService.getAll();
    res.data = data;
    res.statusCode = statusCode;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.getFighterById = (req, res, next) => {
  try {
    const id = { id: req.params.id };
    const { data, statusCode } = FighterService.getById(id);
    res.data = data;
    res.statusCode = statusCode;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createFighter = (req, res, next) => {
  if (!res.err) {
    try {
      const { data, statusCode } = FighterService.create(req.body);
      res.data = data;
      res.statusCode = statusCode;
    } catch (err) {
      res.err = err;
    }
  }
  next();
};

exports.updateFighter = (req, res, next) => {
  if (!res.err) {
    try {
      const { data, statusCode } = FighterService.update(
        req.params.id,
        req.body
      );
      res.data = data;
      res.statusCode = statusCode;
    } catch (err) {
      res.err = err;
    }
  }
  next();
};

exports.deleteFighter = (req, res, next) => {
  try {
    const { data, statusCode } = FighterService.delete(req.params.id);
    res.data = data;
    res.statusCode = statusCode;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};
